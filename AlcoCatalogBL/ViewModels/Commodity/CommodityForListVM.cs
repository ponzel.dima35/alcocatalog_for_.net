﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlcoCatalog.ViewModels.Commodity
{
    public class CommodityForListVM
    {
        public string Name { get; set; }
        public string PictureURL { get; set; }
    }
}
