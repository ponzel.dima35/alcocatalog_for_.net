﻿using AlcoCatalog.ViewModels.Commodity;
using AlcoCatalog.ViewModels.Commodity.Filters;
using System;
using System.Collections.Generic;
using System.Text;

namespace AlcoCatalogBL.ViewModels.Commodity
{
    public class CommodityListVM
    {
        public Guid CategoryId { get; set; }
        public List<AbstractFilterVM> Filters { get; set; }
        public FiltersVM FiltersModel { get; set; }
        public List<CommodityForListVM> Commodities { get; set; }
        public int MyProperty { get; set; }
        public CommodityListVM()
        {
            Filters = new List<AbstractFilterVM>();
            Commodities = new List<CommodityForListVM>();
            FiltersModel = new FiltersVM();
        }

    }
}
