﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlcoCatalog.ViewModels.Commodity.Filters
{
    public class BoolFilterVM : AbstractFilterVM
    {
        public bool Value { get; set; }
    }
}
