﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlcoCatalog.ViewModels.Commodity.Filters
{
    public class AbstractFilterVM
    {
        public string Name { get; set; }
        public string Characteristic { get; set; }
        public bool IsActive { get; set; }
    }
}
