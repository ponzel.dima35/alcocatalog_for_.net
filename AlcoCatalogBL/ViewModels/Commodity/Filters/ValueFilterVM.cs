﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlcoCatalog.ViewModels.Commodity.Filters
{
    public class ValueFilterVM : AbstractFilterVM
    {
        public List<string> Values { get; set; }
        public List<string> AvailableValues { get; set; }
    }
}
