﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AlcoCatalog.ViewModels.Commodity.Filters
{
    public class FiltersVM
    {
        public FiltersVM()
        {
            RangeFilters = new List<RangeFilterVM>();
            ValueFilters = new List<ValueFilterVM>();
            BoolFilters = new List<BoolFilterVM>();
        }
        public List<RangeFilterVM> RangeFilters { get; set; }
        public List<ValueFilterVM> ValueFilters { get; set; }
        public List<BoolFilterVM> BoolFilters { get; set; }
    }
}
