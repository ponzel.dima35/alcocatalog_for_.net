﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlcoCatalog.ViewModels.Commodity.Filters
{
    public class RangeFilterVM : AbstractFilterVM
    {
        public double LowestValue { get; set; }
        public double HigherValue { get; set; }
        public double MaxAvailableValue { get; set; }
        public double MinAvailableValue { get; set; }
    }
}
