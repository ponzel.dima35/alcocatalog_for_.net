﻿using System;
using System.Collections.Generic;

namespace AlcoCatalogBL.DataHendlers.Interfaces
{
    public interface IBaseDataHendler<T> where T : class
    {
        public T GetOne(Guid id);
        public List<T> GetAll();
    }
}
