﻿using AlcoCatalogBL.Models;
using AlcoCatalogBL.Utils;
using AlcoCatalogBL.Utils.Filters;
using System;
using System.Collections.Generic;
using System.Text;

namespace AlcoCatalogBL.DataHendlers.Interfaces
{
    public interface ICommodityDataHendler : IBaseDataHendler<Commodity>
    {
        public Commodity GetCommodity(Guid id);
        public List<Commodity> GetRange(int firstIndex, int conunt, List<AbstractFilter> filters, CommoditySorter sorter);
        public Dictionary<Guid, string> GetCategories();
        public Guid GetCategoryId(Guid commodityId);
    }
}
