﻿using AlcoCatalogBL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AlcoCatalogBL.DataHendlers.Interfaces
{
    public interface ICharacteristicDataHendler : IBaseDataHendler<Characteristic>
    {
        public Guid GetCharacteristicId(string characteristicName, Guid categoryId);
        public string GetCharacteristicValue(Guid commodityId, Guid characteristicId);
        public string GetCharacteristicValueDateType(Guid characteristicId);
        public List<FilterModel> GetFilters(Guid categoryId);
        public List<string> GetAvailableValues(Guid characteristicId);
        public (string value, string dateType) GetMinAvailableValue(Guid characteristicId);
        public (string value, string dateType) GetMaxAvailableValue(Guid characteristicId);
        public List<Characteristic> GetCharacteristicsByCategory(Guid categoryId);
    }
}
