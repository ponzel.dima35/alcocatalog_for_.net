﻿using AlcoCatalogBL.Data;
using AlcoCatalogBL.DataHendlers.Interfaces;
using AlcoCatalogBL.Exceptions;
using AlcoCatalogBL.Models;
using AlcoCatalogBL.Utils;
using AlcoCatalogBL.Utils.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlcoCatalogBL.DataHendlers.RAMDataHandlers
{
    public class CommodityRAMDataHendler : ICommodityDataHendler
    {
        private readonly RAMDataStorage storage = RAMDataStorage.GetInstance();
        public List<Commodity> GetAll()
            => storage._commodities;
        

        public Dictionary<Guid, string> GetCategories()
        {
            var result = new Dictionary<Guid, string>();
            storage._categories.ForEach(c => result.Add(c.Id, c.Name));
            return result;
        }

        public Guid GetCategoryId(Guid commodityId)
            => storage._commodities.FirstOrDefault(c 
                => c.Id == commodityId)?.CategoryId ?? Guid.Empty;

        public Commodity GetCommodity(Guid id)
        {
            var commodity = storage._commodities
                .FirstOrDefault(c => c.Id == id) 
                    ?? throw new CommodityNotFoundException(id);

            commodity.Characteristics =
               storage._commodityCharacteristics.Where(cc => cc.CommodityId == commodity.Id).ToList();
            commodity.Characteristics.ForEach(cc => cc.Characteristic = storage._characteristics
                .FirstOrDefault(ch => ch.Id == cc.CharacteristicId));
            commodity.Category =
                storage._categories.FirstOrDefault(c => c.Id == commodity.CategoryId);
            commodity.Responses =
                storage._responses.Where(r => r.CommodityId == commodity.Id).ToList();
            commodity.BusinessNetworks =
                storage._commoditiesInBusinessNetwork.Where(cbn => cbn.CommodityId == commodity.Id).ToList();
            commodity.BusinessNetworks.ForEach(cbn => cbn.BusinessNetwork = storage._businessNetworks
                .FirstOrDefault(bn => bn.Id == cbn.BusinessNetworkId));
            return commodity;
        }


        public Commodity GetOne(Guid id)
            => storage._commodities.FirstOrDefault(c => c.Id == id);
        public Characteristic GetCharacteristic(Guid id)
            => storage._characteristics.FirstOrDefault(c => c.Id == id);
        public CommodityCharacteristic GetCommodityCharacteristic(Guid id)
            => storage._commodityCharacteristics.FirstOrDefault(c => c.Id == id);
        public Category GetCategory(Guid id)
            => storage._categories.FirstOrDefault(c => c.Id == id);

        public List<Commodity> GetRange(int firstIndex, int conunt, List<AbstractFilter> filters, CommoditySorter sorter)
        {
            var range = storage._commodities;
            foreach (Commodity commodity in range)
            {
                commodity.Characteristics =
                    storage._commodityCharacteristics.Where(cc => cc.CommodityId == commodity.Id).ToList();
                commodity.Characteristics.ForEach(cc => cc.Characteristic = storage._characteristics
                    .FirstOrDefault(ch => ch.Id == cc.CharacteristicId));
            }
            return range
                .Where(c => filters.All(f => f.Filter(c)))
                .ToList().Sort(sorter).Skip(firstIndex).Take(conunt).ToList();
        }
    }
}
