﻿using AlcoCatalogBL.Data;
using AlcoCatalogBL.DataHendlers.Interfaces;
using AlcoCatalogBL.Exceptions;
using AlcoCatalogBL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlcoCatalogBL.DataHendlers.RAMDataHendlers
{
    public class CharacteristicRAMDataHendler : ICharacteristicDataHendler
    {
        private readonly RAMDataStorage storage = RAMDataStorage.GetInstance();
        public List<Characteristic> GetAll()
            => storage._characteristics;

        public List<string> GetAvailableValues(Guid characteristicId)
            => storage._characteristicValues
                .Where(chv => chv.CharacteristicId == characteristicId)
                .Select(chv => chv.Value).ToList();

        public Guid GetCharacteristicId(string characteristicName, Guid categoryId)
            => storage._characteristics
                .FirstOrDefault(ch => ch.Name == characteristicName && (ch.CategoryId == categoryId || ch.CategoryId == null))?.Id ?? 
                    throw new CharacteristicNotFound(characteristicName, categoryId);

        public List<Characteristic> GetCharacteristicsByCategory(Guid categoryId)
            => storage._characteristics
                .Where(ch => ch.CategoryId == categoryId).ToList();

        public string GetCharacteristicValue(Guid commodityId, Guid characteristicId)
            => storage._commodityCharacteristics
                .FirstOrDefault(cc => cc.CommodityId == commodityId
                        && characteristicId == cc.CharacteristicId)?.Value;

        public string GetCharacteristicValueDateType(Guid characteristicId)
            => storage._characteristics
                .FirstOrDefault(cc => cc.Id == characteristicId)?.DataType;

        public List<FilterModel> GetFilters(Guid categoryId)
        {
            var filters = storage._filters;
            foreach (var filter in filters)
            {
                filter.Characteristic =
                    storage._characteristics.FirstOrDefault(c => c.Id == filter.CharacteristicId);
                filter.FilterType =
                    storage._filterTypes.FirstOrDefault(ft => ft.Id == filter.FilterTypeId);
            }
            return filters
                .Where(f => f.Characteristic.CategoryId == null || 
                (f.Characteristic.CategoryId == categoryId && categoryId == Guid.Empty)).ToList();
        }

        public (string value, string dateType) GetMaxAvailableValue(Guid characteristicId)
        {
            var value = storage._characteristicValues
                .Where(chv => chv.CharacteristicId == characteristicId)
                .Max(chv => chv.Value);
            if (string.IsNullOrEmpty(value))
                throw new ValueNotFoundException(characteristicId);

            var dataType = storage._characteristics
                .FirstOrDefault(ch => ch.Id == characteristicId)
                ?.DataType;
            if (string.IsNullOrEmpty(value))
                throw new CharacteristicNotFound(characteristicId);

            return (value, dataType);
        }

        public (string value, string dateType) GetMinAvailableValue(Guid characteristicId)
        {
            var value = storage._characteristicValues
                .Where(chv => chv.CharacteristicId == characteristicId)
                .Min(chv => chv.Value);
            if (string.IsNullOrEmpty(value))
                throw new ValueNotFoundException(characteristicId);

            var dataType = storage._characteristics
                .FirstOrDefault(ch => ch.Id == characteristicId)
                ?.DataType;
            if (string.IsNullOrEmpty(value))
                throw new CharacteristicNotFound(characteristicId);

            return (value, dataType);
        }

        public Characteristic GetOne(Guid id)
            => storage._characteristics.FirstOrDefault(ch => ch.Id == id);
    }
}
