﻿using AlcoCatalog.ViewModels.Commodity;
using AlcoCatalog.ViewModels.Commodity.Filters;
using AlcoCatalogBL.DataHendlers.Interfaces;
using AlcoCatalogBL.Exceptions;
using AlcoCatalogBL.Models;
using AlcoCatalogBL.Services.Interfaces;
using AlcoCatalogBL.Utils;
using AlcoCatalogBL.Utils.Filters;
using AlcoCatalogBL.ViewModels.Commodity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlcoCatalogBL.Services
{
    class CommodityService : ICommodityService
    {
        private readonly ICommodityDataHendler _commodities;
        private readonly ICharacteristicDataHendler _characteristics;
        public CommodityService(ICommodityDataHendler commodities, ICharacteristicDataHendler characteristics)
        {
            _commodities = commodities;
            _characteristics = characteristics;
        }

        public Dictionary<Guid, string> GetCategories()
            => _commodities.GetCategories();

        public Commodity GetCommodity(Guid id)
            => _commodities.GetCommodity(id);

        public void GetRange(int pageNumber, int countInPage, ref CommodityListVM model, CommoditySorter sorter)
        {
            var filters = CreateFilters(ref model);
            var commodities = _commodities.GetRange(pageNumber * countInPage, countInPage, filters, sorter);
            model.Commodities = MapCommodities(commodities);
        }

        public List<AbstractFilter> CreateFilters(ref CommodityListVM model)
        {
            var filters = new List<AbstractFilter>();
            var availableFilters = _characteristics.GetFilters(model.CategoryId);

            foreach (FilterModel filterModel in availableFilters)
            {
                AbstractFilterVM filterVM = model.Filters.FirstOrDefault(fvm => fvm.Name == filterModel.Name);
                if (filterVM is null)
                    model.Filters.Add(CreateOne(filterModel));
                else
                    filters.Add(CreateOne(filterVM));
            }
            return filters;
        }

        public AbstractFilterVM CreateOne(FilterModel filterModel)
        {
            Type filterVMType = (Type.GetType($"AlcoCatalog.ViewModels.Home.CommodityList.Filters.{filterModel.FilterType.Name}VM", true, false));
            var filterVM = Activator.CreateInstance(filterVMType) as AbstractFilterVM;
            if (filterVM.GetType() == typeof(RangeFilterVM))
            {
                var minValue = _characteristics.GetMinAvailableValue(filterModel.CharacteristicId);
                ((RangeFilterVM)filterVM).MinAvailableValue =
                    (double)minValue.value.ParseCharacteristic(minValue.dateType);
                var maxValue = _characteristics.GetMaxAvailableValue(filterModel.CharacteristicId);
                ((RangeFilterVM)filterVM).MaxAvailableValue =
                    (double)maxValue.value.ParseCharacteristic(maxValue.dateType);
            }
            if (filterVM.GetType() == typeof(ValueFilterVM))
            {
                ((ValueFilterVM)filterVM).AvailableValues = _characteristics.GetAvailableValues(filterModel.CharacteristicId);
            }
            filterVM.Characteristic = filterModel.Characteristic.Name;
            filterVM.Name = filterModel.Characteristic.Name;
            return filterVM as AbstractFilterVM;
        }
        public AbstractFilter CreateOne(AbstractFilterVM filterVM)
        {
            if (!filterVM.IsActive) return null;
            AbstractFilter result = null;
            if (filterVM.GetType() == typeof(ValueFilterVM))
                result = new ValueFilter(_characteristics, filterVM.Characteristic, ((ValueFilterVM)filterVM).Values);
            else if (filterVM.GetType() == typeof(RangeFilterVM))
                result = new RangeFilter(_characteristics, filterVM.Characteristic, ((RangeFilterVM)filterVM).LowestValue, ((RangeFilterVM)filterVM).HigherValue);
            else if (filterVM.GetType() == typeof(BoolFilterVM))
                result = new BoolFilter(_characteristics, filterVM.Characteristic, ((BoolFilterVM)filterVM).Value);
            else
                throw new FilterNotFoundExceptions(filterVM.GetType());

            return result;
        }

        public Dictionary<Guid, object> GetAllCharacteristicsValues(Guid commodityId)
        {
            var categoryId = _commodities.GetCategoryId(commodityId);
            if (categoryId != Guid.Empty)
                throw new CommodityNotFound(commodityId);

            var characteristicsIds = _characteristics.GetCharacteristicsByCategory(categoryId)
                .Select(ch => ch.Id).ToList();

            return GetCharacteristicsValues(commodityId, characteristicsIds);
        }

        public object GetCharacteristicValue(Guid commodityId, Guid characteristicId)
        {
            var characteristic = _characteristics.GetOne(characteristicId);
            if (characteristic is null)
                throw new CharacteristicNotFound(characteristicId);

            string value = _characteristics.GetCharacteristicValue(commodityId, characteristic.Id);
            if (value is null)
                throw new CharacteristicIsNotForThisCommodityAndCtegory(characteristicId, commodityId, characteristic.CategoryId);
            if (value == string.Empty)
                throw new CharacteristicValueIsEmpty(characteristicId, commodityId, characteristic.CategoryId);
            return value.ParseCharacteristic(characteristic.DataType);
        }
        public Dictionary<Guid, object> GetCharacteristicsValues(Guid commodityId, List<Guid> characteristicsIds)
        {
            var result = new Dictionary<Guid, object>();
            characteristicsIds.ForEach(c =>
            {
                try
                {
                    result.Add(c, GetCharacteristicValue(commodityId, c));
                }
                catch (CharacteristicValueIsEmpty) { } //add logs
            });

            return result;
        }

        private List<CommodityForListVM> MapCommodities(List<Commodity> commodities)
        {
            var result = new List<CommodityForListVM>();
            foreach (var commodity in commodities)
            {
                result.Add(MapCommodity(commodity));
            }
            return result;
        }

        private CommodityForListVM MapCommodity(Commodity commodity) =>
            new CommodityForListVM
            {
                Name = commodity.Name,
                PictureURL = commodity.PictureURL
            };
    }
}
