﻿using AlcoCatalogBL.Models;
using AlcoCatalogBL.Utils;
using AlcoCatalogBL.ViewModels.Commodity;
using System;
using System.Collections.Generic;
using System.Text;

namespace AlcoCatalogBL.Services.Interfaces
{
    public interface ICommodityService
    {
        public void GetRange(int pageNumber, int countInPage, ref CommodityListVM model, CommoditySorter sorter);
        public Dictionary<Guid, string> GetCategories();
        public Commodity GetCommodity(Guid id);

    }
}
