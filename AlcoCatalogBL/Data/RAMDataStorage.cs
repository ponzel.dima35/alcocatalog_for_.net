﻿using AlcoCatalogBL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AlcoCatalogBL.Data
{
    public class RAMDataStorage
    {
        private static RAMDataStorage instance;
        public static RAMDataStorage GetInstance()
        {
            if (instance == null)
                instance = new RAMDataStorage();
            return instance;
        }
        public RAMDataStorage()
        {
            _characteristicValues = new List<CharacteristicValue>();
            _characteristics = new List<Characteristic>();
            _commodityCharacteristics = new List<CommodityCharacteristic>();
            _commodities = new List<Commodity>();
            _categories = new List<Category>();
            _filters = new List<FilterModel>();
            _filterTypes = new List<FilterTypeModel>();
            _responses = new List<Response>();
            _businessNetworks = new List<BusinessNetwork>();
            _commoditiesInBusinessNetwork = new List<CommodityInBusinessNetwork>();
        }
        public List<CharacteristicValue> _characteristicValues;
        public List<Characteristic> _characteristics;
        public List<CommodityCharacteristic> _commodityCharacteristics;
        public List<Commodity> _commodities;
        public List<Category> _categories;
        public List<FilterModel> _filters;
        public List<FilterTypeModel> _filterTypes;
        public List<Response> _responses;
        public List<BusinessNetwork> _businessNetworks;
        public List<CommodityInBusinessNetwork> _commoditiesInBusinessNetwork;
    }
}
