﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlcoCatalogBL.Models
{
    public class BusinessNetwork : IBaseObject
    {
        public Guid Id { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string Name { get; set; }
        public string SiteURL { get; set; }
        public string PictureURL { get; set; }
        public List<CommodityInBusinessNetwork> Commodities { get; set; }
        public List<CharacteristicInBusinessNetwork> CharacteristicsInBusinessNetwork { get; set; }

    }
}
