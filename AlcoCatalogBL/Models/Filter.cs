﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlcoCatalogBL.Models
{
    public class FilterModel : IBaseObject
    {
        public Guid Id { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string Name { get; set; }
        public Characteristic Characteristic { get; set; }
        public Guid CharacteristicId { get; set; }
        public FilterTypeModel FilterType { get; set; }
        public Guid FilterTypeId { get; set; }
    }
}
