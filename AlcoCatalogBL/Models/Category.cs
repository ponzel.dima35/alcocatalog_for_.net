﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlcoCatalogBL.Models
{
    public class Category : IBaseObject
    {
        public Guid Id { get; set; }
        public Guid? CreatedBy { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public double LowestAlcoholContains { get; set; }
        public double HighestAlcoholContains { get; set; }
        public List<Commodity> Commodities { get; set; }
        public List<Characteristic> Characteristics { get; set; }
    }
}
