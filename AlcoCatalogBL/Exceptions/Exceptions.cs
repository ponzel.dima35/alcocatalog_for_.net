﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AlcoCatalogBL.Exceptions
{
    public class CharacteristicNotFound : Exception
    {
        public CharacteristicNotFound(Guid characteristicId)
            : base($"Characteristic with Id = {characteristicId.ToString()} not found")
        { }

        public CharacteristicNotFound(string characteristicName, Guid categoryId)
            : base($"Characteristic with Name = {characteristicName} and Category with Id = {categoryId.ToString()} not found")
        { }
        public CharacteristicNotFound(Guid characteristicId, Guid commodityId, Guid categoryId)
            : base($"Characteristic with Id = {characteristicId.ToString()} is not for " +
          $"Commodity with Id = {commodityId.ToString()} and Characteristic with Id = {categoryId.ToString()}")
        { }
    }
    public class CharacteristicIsNotForThisCommodityAndCtegory : Exception
    {
        public CharacteristicIsNotForThisCommodityAndCtegory(Guid characteristicId, Guid commodityId, Guid? categoryId)
            : base($"Characteristic with Id = {characteristicId.ToString()} is not for " +
          $"Commodity with Id = {commodityId.ToString()} and Characteristic with Id = {categoryId?.ToString()}")
        { }
    }
    public class CharacteristicValueIsEmpty : Exception
    {
        public CharacteristicValueIsEmpty(Guid characteristicId, Guid commodityId, Guid? categoryId)
            : base($"Characteristic with Id = {characteristicId.ToString()}, " +
                  $"Commodity with Id = {commodityId.ToString()} and Characteristic with Id = {categoryId?.ToString()} " +
                  $"is empty")
        { }
    }
    public class CharacteristicValueParsingException : Exception
    {
        public CharacteristicValueParsingException(string value, string dataType)
            : base($"Parsing error. Parsing value = {value} to date type {dataType}")
        { }
    }
    public class CommodityNotFound : Exception
    {
        public CommodityNotFound(Guid commodityId)
            : base($"Commodity with Id = {commodityId.ToString()} not found")
        { }
    }
    public class ValueNotFoundException : Exception
    {
        public ValueNotFoundException(Guid characteristicId)
            : base($"No one characteristic value with id = {characteristicId.ToString()} was not found in table CharacteristicValues")
        { }
    }
    public class CommodityNotFoundException : Exception
    {
        public CommodityNotFoundException(Guid id)
            : base($"Commodity with Id = {id.ToString()} not found")
        { }
    }
    public class FilterNotFoundExceptions : Exception
    {
        public FilterNotFoundExceptions(Type filterVMType)
            : base($"Filter with VM type = {filterVMType.ToString()} not found")
        { }
    }
}
