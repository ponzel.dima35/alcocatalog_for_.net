﻿using AlcoCatalogBL.Exceptions;
using AlcoCatalogBL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlcoCatalogBL.Utils
{
    public static class ExtensionMethods
    {
        public static List<Commodity> Sort(this List<Commodity> list, CommoditySorter sorter)
        {
            if (sorter._orderVector == OrderVector.Asc)
                return list.OrderBy(sorter.Function).ToList();
            return list.OrderByDescending(sorter.Function).ToList();
        }
        public static object ParseCharacteristic(this string value, string dataType)
        {
            switch (dataType)
            {
                case "DOUBLE":
                    if (Double.TryParse(value = value.Replace('.', ','), out double double_result))
                        return double_result;
                    break;
                case "DATETIME":
                    if (DateTime.TryParse(value, out DateTime datetime_result))
                        return datetime_result;
                    break;
                case "INT":
                    if (Int32.TryParse(value, out int int_result))
                        return int_result;
                    break;
                case "STRING":
                    return value;
                default:
                    throw new CharacteristicValueParsingException(value, dataType);
            }
            return null;
        }
    }
}
