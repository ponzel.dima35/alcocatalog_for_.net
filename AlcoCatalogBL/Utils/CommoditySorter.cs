﻿
using AlcoCatalogBL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlcoCatalogBL.Utils
{
    public class CommoditySorter
    {
        public readonly OrderType _orderType;
        public readonly OrderVector _orderVector;
        public Func<Commodity, object> Function { get; private set; }
        public CommoditySorter(OrderType orderType, OrderVector orderVector)
        {
            _orderType = orderType;
            _orderVector = orderVector;
            switch (_orderType)
            {
                case OrderType.AlcoholPercent:
                    Function = x => x.Characteristics.FirstOrDefault(c => c.Characteristic.Name == "AlcoholPercent").Value;
                    break;
            }
        }

    }
    public enum OrderType
    {
        Date,
        Price,
        AlcoholPercent
    }
    public enum OrderVector
    {
        Desc,
        Asc
    }
}
