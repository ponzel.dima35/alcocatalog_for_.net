﻿using AlcoCatalogBL.DataHendlers.Interfaces;
using AlcoCatalogBL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlcoCatalogBL.Utils.Filters
{
    public class BoolFilter : AbstractFilter
    {
        private readonly bool _value;
        public BoolFilter(ICharacteristicDataHendler repository, string characteristicName,
            bool value = false, bool isActive = true)
                : base(characteristicName, repository, isActive)
        {
            _value = value;
        }
        public override bool Filter(Commodity commodity)
        {
            if (!_value) return true; 
            var characteristicId = _repository.GetCharacteristicId(_characteristicName, commodity.CategoryId);
            bool value = (bool)_repository.GetCharacteristicValue(commodity.Id, characteristicId)
                .ParseCharacteristic(_repository.GetCharacteristicValueDateType(characteristicId));
            return value;
        }
    }
}
