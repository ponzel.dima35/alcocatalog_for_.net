﻿using AlcoCatalogBL.DataHendlers.Interfaces;
using AlcoCatalogBL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlcoCatalogBL.Utils.Filters
{
    public class RangeFilter : AbstractFilter
    {
        private readonly double _lowestLimit;
        private readonly double _highestLimit;
        public RangeFilter(ICharacteristicDataHendler repository, string characteristicName,
            double lowestLimit = 0, double highestLimit = double.MaxValue, bool isActive = true)
                : base(characteristicName, repository, isActive)
        {
            _lowestLimit = lowestLimit;
            _highestLimit = highestLimit;
        }
        public override bool Filter(Commodity commodity)
        {
            if (_lowestLimit == 0 && _highestLimit == double.MaxValue) return true;
            var characteristicId = _repository.GetCharacteristicId(_characteristicName, commodity.CategoryId);
            double value = (double)_repository.GetCharacteristicValue(commodity.Id, characteristicId)
                .ParseCharacteristic(_repository.GetCharacteristicValueDateType(characteristicId));
            return (value >= _lowestLimit && value <= _highestLimit);
        }
    }
}
