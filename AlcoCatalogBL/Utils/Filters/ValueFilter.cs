﻿using AlcoCatalogBL.DataHendlers.Interfaces;
using AlcoCatalogBL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlcoCatalogBL.Utils.Filters
{
    public class ValueFilter : AbstractFilter
    {
        private readonly List<string> _values;
        public ValueFilter(ICharacteristicDataHendler repository, string characteristicName,
            List<string> values, bool isActive = true)
                : base(characteristicName, repository, isActive)
        {
            _values = values;
        }
        public ValueFilter(ICharacteristicDataHendler repository, string characteristicName,
            string value, bool isActive = true)
                : base(characteristicName, repository, isActive)
        {
            _values = new List<string> { value };
        }
        public override bool Filter(Commodity commodity)
        {
            var characteristicId = _repository.GetCharacteristicId(_characteristicName, commodity.CategoryId);
            string value = (string)_repository.GetCharacteristicValue(commodity.Id, characteristicId);
            return _values.Any(v => v == value);
        }

    }
}
