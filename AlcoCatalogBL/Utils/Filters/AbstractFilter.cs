﻿using AlcoCatalogBL.DataHendlers.Interfaces;
using AlcoCatalogBL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlcoCatalogBL.Utils.Filters
{
    public abstract class AbstractFilter
    {
        protected readonly ICharacteristicDataHendler _repository;
        protected readonly string _characteristicName;
        public AbstractFilter(string characteristicName, ICharacteristicDataHendler repository, bool isActive = true)
        {
            _characteristicName = characteristicName;
            _repository = repository;
            IsActive = isActive;
        }

        public bool IsActive { get; protected set; }

        public abstract bool Filter(Commodity commodity);
    }
}
